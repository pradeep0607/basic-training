// Problem 1: Complete the secondLargest function which takes in an array of numbers in input and return the second biggest number in the array. (without using sort)?
function secondLargest(array) {
  let length = array.length;                                  //checking array length
  let firstLargeNum = 0;  
  let secondLargeNum = 0;
  
   for(const item of array){                                   // iterating loop over the iable objects
      if(firstLargeNum<item)
      {   
        secondLargeNum = firstLargeNum;                   
        firstLargeNum = item;       
      }
      else if(item>secondLargeNum && item<firstLargeNum)
      {
        secondLargeNum = item;
      }
   }
  return secondLargeNum;
}
  // Problem 2: Complete the calculateFrequency function that takes lowercase string as input and returns alphabetFrequency of all english alphabet. (using only array, no in-built function)
  function calculateFrequency(string) {
    let alphabetFrequency = {};                          //  creating a blank set of objects 
    for (let i = 0; i < string.length; i++) {            // loop for checking length of string 
      let character = string.charAt(i);                  // check the index of character  in string 
      if (character >= "a" && character <= "z") {
        if (alphabetFrequency[character]){               //loop for incrementing the frequency from 0   
            alphabetFrequency[character]++;              // incrementing the charater frequency
         }
        else{
        alphabetFrequency[character] = 1;  
         }
      }
    }
      return alphabetFrequency;
  }
  
  // Problem 3: Complete the flatten function that takes a JS Object, returns a JS Object in flatten format (compressed)
  function flatten(unflatObject) {
      let flatObj = {};                                       //  creating a blank set of objects       
      
      for (let i in unflatObject) {                        //iating loop
  
        if ((typeof unflatObject[i]) == 'object') {        //return the type of object i.e "number" string ,boolean,undefined
       
          let flatObject = flatten(unflatObject[i]);       //creating an another object and assign function to it  
          for (let l in flatObject) {                      // iating loop for flatobjest 
  
            flatObj[i + '.' + l] = flatObject[l];             // append the string with the .(dot) 
          }
        } else {                                       
          flatObj[i] = unflatObject[i];     
        }
      }
      return flatObj;                                          // finally print the flatted string 
      
  }
  
  // Problem 4: Complete the unflatten function that takes a JS Object, returns a JS Object in unflatten format
  function unflatten(flatObject) {
    let unflat = {}, temp, splitItems, key;                      //creating empty set object and  other variable for spliting 
      for (key in flatObject) {                                  // iating loop for key and flatobjects
          splitItems = key.split('.');                           // here we are spliting the string with dot (.) 
          temp = unflat;                                         // assigning the unflated sets to a temp variable     
          for (let a = 0; a < splitItems.length - 1; a++) {      // checking the lenght of spliteditem and variable 
              if (!(splitItems[a] in temp)) {                    // loop for not splited item which is stored in temp variable 
                  if (typeof splitItems[a + 1] ) {               // checking type of splited item
                      temp[splitItems[a]] = [];      
                  }
                   else {
                      temp[splitItems[a]] = {};      
                  }
              }
              temp = temp[splitItems[a]];                             // storing the uflated set into temp variable
          }
          temp[splitItems[splitItems.length - 1]] = flatObject[key];  // finally convertin sets into a json formate
      }
      return unflat;
  }
